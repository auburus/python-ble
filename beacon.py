'''
Advertise a Beacon for a minute, and then disapear
'''
from bluetooth.ble import BeaconService
import ctypes
import time

service = BeaconService()

# start_advertising(string uuid, int major, int minor, int txpower, int interval)
service.start_advertising("2e5128ef-ccec-4ed5-a88f-00ab96de61ec", 1, 1, -30, 200)

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    service.stop_advertising()
