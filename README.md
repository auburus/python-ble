Quickstart quide
===============

For that guide, I am assuming than debian Jessie is being used (although it probably works
with ubuntu and similars, and newer versions of debian).
Install the required packages:
```bash
sudo aptitude install git python3.4 python3.4-dev python3-pip pkg-config libboost-python-dev \
libboost-thread-dev libbluetooth-dev libglib2.0-dev bluetooth libbluetooth3

pip3 install virtualenv
```

Move to the foler where you want to clone the project, and run
```bash
git clone https://gitlab.com/auburus/python-ble.git
cd python-ble
python3 -m virtualenv --python=/usr/bin/python3.4 venv
source ./venv/bin/activate
pip install -r requirements.txt
```

With all that, the programs shoud work directly just by runing 
`python <name_of_the_program.py>`.
